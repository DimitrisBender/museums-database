# Environment
MKDIR=mkdir
CC=gcc
AS=as

# Macros
OBJECTDIR=build
SRCDIR=src
# Object Files
OBJECTFILES= \
	${OBJECTDIR}/Controller.o \
	${OBJECTDIR}/linkedlist.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

all:museums_exec

museums_exec: ${OBJECTFILES}
	${LINK.c} -o museums_exec ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/Controller.o: ${SRCDIR}/Controller.c ${SRCDIR}/proj_header.h ${SRCDIR}/linked_list.h
	${MKDIR} -p ${OBJECTDIR}
	$(COMPILE.c) -g -Wall -MMD -MP -o ${OBJECTDIR}/Controller.o ${SRCDIR}/Controller.c

${OBJECTDIR}/linkedlist.o: ${SRCDIR}/linkedlist.c ${SRCDIR}/linked_list.h 
	${MKDIR} -p ${OBJECTDIR}
	$(COMPILE.c) -g -Wall -MMD -MP -o ${OBJECTDIR}/linkedlist.o ${SRCDIR}/linkedlist.c

${OBJECTDIR}/main.o: ${SRCDIR}/main.c ${SRCDIR}/proj_header.h
	${MKDIR} -p ${OBJECTDIR}
	$(COMPILE.c) -g -Wall -MMD -MP -o ${OBJECTDIR}/main.o ${SRCDIR}/main.c

clean: .clean-conf

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} museums_exec
	${RM} -r ${OBJECTDIR}

# Subprojects
.clean-subprojects:
