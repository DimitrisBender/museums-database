/*
 * File:   linked_list.h
 * Author: Dimitrios Lygizos
 *
 * Created on October 4, 2014, 9:20 PM
 */

#ifndef LINKEDLIST_H
#define	LINKEDLIST_H

#include <stdio.h>
#include <stdlib.h>

typedef struct List_Node {
    void *data;
    struct List_Node *next;
} node;

/*
 * Create an element based on given argument
 * Insert it at list front
 */
void Push( node** headRef, void *data );

/*
 * Alternative push function which takes a prepared valid node pointer
 * and push it at the head of the list
 */
void alt_Push(node **headRef, node* newNode);

/*
 * given a list that is sorted in increasing order and
 * a single node, inserts this node into the correct sorted position in the list.
 * SortedInsert() takes an existing node, and just rearranges pointers
 * to insert it into the list.
 */
void SortedInsert( node** headRef, node* newNode, int (*cmpfunc) (void *, void *) );

/*
 * Given an integer argument -index
 * Insert the given argument at the index position in the list
 */
void InsertNth( node** headRef, int index, void *data );

/*
 * Given a list and an index, return the node
 * in the nth node of the list.
 * Assert fails if the index is invalid (outside 0..lengh-1).
 */
node *GetNth( node* head, int index );

/*
 * The opposite of Push(). Takes a non-empty list
 * and removes the front node, and returns the data
 * which was in that node.
 */
void *Pop( node** headRef );

/*
 * print the contents of the list
 * Required: head of the list and a pointer to a function
 * which has the duty to print a specific node* element
 */
void printContents( node* head, void (*printfunc) () );

/*
Given a linked list head pointer, compute
and return the number of nodes in the list.
*/
int Length( node* head );

/*
 * find a specific element in the list
 * based on the element argument.
 * cmpfunc function argument is an argument which:
 * Returns: -1 if second argument is greater than the first
 * 0 if the arguments are equal
 * 1 if the first argument is greater than the second
 */
node* find( node *head, void *element, int (*cmpfunc)(void *, void *) );

/*
 * Given a list and an integer argument, return the number of times
 * that integer occurs in the list.
 * cmpfunc function argument is an argument which:
 * Returns: -1 if second argument is greater than the first
 * 0 if the arguments are equal
 * 1 if the first argument is greater than the second
 */
int Count( node* head, void *searchFor, int (*cmpfunc)(void *, void *) );

/*
 * Remove (and only remove) a specific node in the list based on the given argument -element
 * cmpfunc function argument is an argument which:
 * Returns: -1 if second argument is greater than the first
 * 0 if the arguments are equal
 * 1 if the first argument is greater than the second
 */
void removeNode(node **headRef, void *element, int (*cmpfunc) (void *, void *));

/* Delete whole list */
void DeleteList( node** headRef );

/*
 * Given a list, change it to be in sorted order (using SortedInsert()).
 * cmpfunc function argument is an argument which:
 * Returns: -1 if second argument is greater than the first
 * 0 if the arguments are equal
 * 1 if the first argument is greater than the second
 */
void InsertSort( node** headRef, int (*cmpfunc) (void *, void *) );

/*
 * Append 'b' onto the end of 'a', and then set 'b' to NULL.
 */
void Append( node** aRef,  node** bRef );

/*
 * Split the nodes of the given list into front and back halves,
 * and return the two lists using the reference parameters.
 * If the length is odd, the extra node should go in the front list.
 * WARNING: This function DOES NOT preserve the source list!!!
*/
void FrontBackSplit( node** source,  node** frontRef,  node** backRef);

/*
 * Given the source list, split its nodes into two shorter lists.
 * If we number the elements 0, 1, 2, ... then all the even elements
 * should go in the first list, and all the odd elements in the second.
 * The elements in the new lists may be in any order.
 * WARNING: This function DOES NOT preserve the source list!!!
*/
void AlternatingSplit( node** source, node** aRef, node** bRef );

/*
 * Remove duplicates from a list
 * cmpfunc function argument is an argument which:
 * Returns: -1 if second argument is greater than the first
 * 0 if the arguments are equal
 * 1 if the first argument is greater than the second
 */
void RemoveDuplicates( node* head, int (*cmpfunc) (void *, void *) );

/*
 * Remove any duplicates from a sorted list.
 * Note: Single traversal efficiency
 * cmpfunc function argument is an argument which:
 * Returns: -1 if second argument is greater than the first
 * 0 if the arguments are equal
 * 1 if the first argument is greater than the second
 */
void RemoveDuplicatesSorted( node *head, int (*cmpfunc) (void *, void *) );

/*
 * Take the node from the front of the source,
 * and move it to the front of the dest.
 * It is an error to call this with the source list empty.
 */
void MoveNode( node** destRef,  node** sourceRef );

/*
 * Merge the nodes of the two lists into a single list taking a node
 * alternately from each list, and return the new list. [recursion]
 * WARNING: This function DOES NOT preserve the source lists!!!
*/
node* ShuffleMerge(node** a,  node** b);

/*
 * Takes two lists sorted in increasing order, and splices their nodes
 * together to make one big sorted list which is returned.
 * cmpfunc function argument is an argument which:
 * Returns: -1 if second argument is greater than the first
 * 0 if the arguments are equal
 * 1 if the first argument is greater than the second
 * WARNING: This function DOES NOT preserve the source lists!!!
 */
node* SortedMerge(node* a, node* b, int (*cmpfunc) (void *, void *) );

/*
 * NOTE: Recursion!
 * Split the list into two smaller lists, recursively sort those lists,
 * and finally merge the two sorted lists together into a single sorted list.
 * cmpfunc function argument is an argument which:
 * Returns: -1 if second argument is greater than the first
 * 0 if the arguments are equal
 * 1 if the first argument is greater than the second
 */
void MergeSort(node **, int (*cmpfunc) (void *, void *) );

/*
 * Reverse the given linked list by changing its .next pointers
 * and its head pointer. Takes a pointer (reference) to the head pointer.
 */
void Reverse(node **headRef);

/*
 * Recursively reverses the given linked list by changing its .next
 * pointers and its head pointer in one pass of the list.
 */
void RecursiveReverse(node **headRef);

/*
 * Recursively reverses the given linked list by changing its .next pointers
 * and its head pointer in one pass of the list. 2 arguments way
 */
void RecursiveReverse2(node **, node*);

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* LINKEDLIST_H */