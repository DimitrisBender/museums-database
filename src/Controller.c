/*
 * File:   Controller.c
 * Author: Dimitris Lygizos
 *
 * Created on November 22, 2014, 9:28 AM
 */

#include "proj_header.h"

/* ======================== */
/* Basic function for Double-link list which stores the painting collection */

/*
 * Insert a new painting to the HEAD of the collection
 * @param painting node to push
 */
void paintingPush(PAINTING **headRef, PAINTING *p)
{
	(*headRef)->prev = p;
	p->next = *headRef;
	*headRef = p;
}

/*
 * Delete a painting from the collection
 * @param ID of the painting to delete
 */
void paintingDelete(PAINTING **headRef, unsigned int paintingId)
{
	PAINTING *tmp, *p = *headRef;
	
	if( *headRef == '\0' ) {
		fprintf(stderr, "Request on an empty list!\n");
	} else if( (*headRef)->paintingId == paintingId )
	{
		*headRef = p->next;
		(*headRef)->prev = '\0';
		free(p);
	} else {
		while( p->next )
		{
			if( p->next->paintingId == paintingId )
			{
				tmp = p->next;
				if(tmp->next != '\0')	tmp->next->prev = p;
				p->next = tmp->next;
				free(tmp);
				break;
			}
		}
	}
}

/*
 * Print the painting collection
 */
void paintingsPrint(PAINTING *head)
{
	MUSEUM *m;
	ROOM *r;
	int flN;
	while(head->next)
	{
		r = head->roomPtr;
		m = head->museumPtr;
		for(flN=0; !roomLookup(m->FL[flN], r->roomId); ++flN) {} 	// find the museum floor where the room rests
		fprintf(stdout, "< %u, %s, %u, %s, %u, %s, %u, %u >\n", head->paintingId, head->paintingName,
				head->artistPtr->artistId, head->artistPtr->artistName, m->museumId,
				m->museumName, flN, r->roomId);
		head = head->next;
	}
}

/*
 * Search for a specific painting from the collection
 * @param id of the painting to search
 * @return painting node or null
 */
PAINTING *paintingLookup(PAINTING *head, unsigned int paintingId)
{
	
	while( head->next )
	{
		if( head->paintingId == paintingId ) return head;
		head = head->next;
	}
	return NULL;
}

/* Clear all paintings from the list */
void DeletePaintingsList(PAINTING **head)
{
	PAINTING *current;
	while (*head) {
		current = *head;
		*head = (*head)->next;
		free(current);
	}
	*head = NULL;
}

/* ======================== */
/* Specific list functions  */
node *sorted_list_lookup(node *head, void *element, int (*compare) (void *, void *))
{
	node* current;

	for(current = head; current; current = current->next) {
		if(compare(current->data, element) == 1) break;
		if( !compare(current->data, element) ) return current;
	}

	return NULL;
}

/*
 * Delete a node from a sorted list
 * @param headref - Pointer to head pointer of the list
 * @param element - data to be deleted
 * @param cmpfunc - compare function to determine which node is the valid node
 */
void sorted_list_delete(node **headRef, void *element, int (*cmpfunc) (void *, void *))
{
    node *current, *tmp;
    if(*headRef == NULL) fprintf(stderr, "removeNode: Request on an empty list!\n");
    else {
        current = *headRef;
        if( !cmpfunc((*headRef)->data, element) ) {     // if node-to-delete is list's head
            *headRef = current->next;
            free(current->data);
            free(current);
        } else {
            while(current->next) {
                if( cmpfunc(current->next->data, element) == 1 ) break; // list is sorted, we found a node with greater value
                else if( !cmpfunc(current->next->data, element) ) {     // we found our node
                    tmp = current->next;
                    current->next = tmp->next;
                    free(tmp->data);
                    free(tmp);
                    break;
                } else
                    current = current->next;
            }
        }
    }
}
/* ======================== */
/* Art style list functions */

/*
 * Compare 2 art style object based on their ID - artStyleId
 * Takes 2 arguments a, b and returns 1 if a > b, 0 if a = b, -1 if b > a
 */
int cmp_artStyle(ARTSTYLE *a, ARTSTYLE *b)
{
    if( a == b ) return 0;
    else if ( (*a).artStyleId > (*b).artStyleId ) return 1;
    else if( (*a).artStyleId == (*b).artStyleId ) return 0;
    else return -1;
}

// helper function for printing a single artStyle instance
void artStylePrint(ARTSTYLE* style)
{
    printf("< %u,  %s >\n", style->artStyleId, style->styleName);
}

/*
 * Print the artStyles list
 * Takes as argument the head of the list
 */
void artStylesPrint(node* head)
{
    printf("List of Art Styles:\n");
    printContents(head, artStylePrint);
}

/*
 * Create and Insert a new artStyle instance into the artStyles list
 * Takes as arguments a reference to artStyle head list,
 * the ID of the given and the name the artstyle is going to have
 */
void artStyleInsert(node** headRef, unsigned int artStyleId, char styleName[MAX_NAME_LENGTH])
{
    ARTSTYLE *tmp = (ARTSTYLE *) malloc (sizeof(ARTSTYLE));     // Create new artstyle node
    tmp->artStyleId = artStyleId;
    strncpy( tmp->styleName, styleName, strlen(styleName) );
    node *insertion = (node *) malloc(sizeof(node));            // Create new list node
    insertion->data = tmp;
    alt_Push(headRef, insertion);
    fprintf(stdout, "Successfully inserted art style %s\n", styleName);

}

/*
 * Search for a specific artStyle instance based on his ID and return it.
 * @param head - list's first element
 * @param id of artStyle to find
 * @return corresponding artStyle if exists or NULL
 */
ARTSTYLE *artStyleLookUp(node* head, unsigned int artStyleId)
{
    node *result;
    ARTSTYLE style;                 // TEMP artStyle node
    style.artStyleId = artStyleId;

    if( (result = find(head, &style, cmp_artStyle)) != NULL )	// call the library function find
        return result->data;    // return node in list

    return NULL;
}

/*
 * Delete a specific instance from the artStyles list
 * @param pointer to list head
 * @param id of artStyle to delete
 */
void artStyleDelete(node **headRef, unsigned int artStyleId)
{
    ARTSTYLE style;
    style.artStyleId = artStyleId;
    removeNode(headRef, &style, cmp_artStyle);	// call the library function removeNode
}

/* ====================== */
/* Artists list functions */

/*
 * Compare 2 artist instances
 * Takes 2 arguments a, b and returns 1 if a > b, 0 if a = b, -1 if b > a
 */
int cmp_artist(ARTIST *a, ARTIST *b)
{
    if( a == b ) return 0;
    else if( a->artistId > b->artistId ) return 1;
    else if( a->artistId == b->artistId ) return 0;
    else return -1;
}

// print a single artist instance
void artistPrint(ARTIST *data)
{
    ARTSTYLE *style = (ARTSTYLE *) data->artStylePtr;
    printf("< %u, %s, %u, %d, %u, %s >\n", data->artistId, data->artistName, data->YearOfBirth, data->YearOfDeath, style->artStyleId, style->styleName);
}

// trigger list printing system for artists
void artistsPrint(node* head)
{
    printf("List of Artists:\n");
    printContents(head, artistPrint);
}

/*
 * Add a new artist into artists' list
 */
void artistInsert(node** headRef, unsigned int artistId, unsigned int YearOfBirth, int YearOfDeath, unsigned int artStyleId, node* artStyleList, char name[MAX_NAME_LENGTH])
{
    ARTIST *new = (ARTIST *) malloc(sizeof(ARTIST));
    node *container = (node *) malloc (sizeof(node));
    new->YearOfBirth = YearOfBirth;
    new->YearOfDeath = YearOfDeath;
    new->artistId = artistId;
    strncpy(new->artistName, name, MAX_NAME_LENGTH);
    new->artStylePtr = artStyleLookUp(artStyleList, artStyleId);
    container->data = new;
    container->next = NULL;
    alt_Push(headRef, container);
    fprintf(stdout, "Successfully inserted artist %s\n", name);
}

/*
 * search for a specific artist
 * @param pointer to first element in the list
 * @param id of artist to look for
 * @return corresponding artist node in the list OR NULL
 */
ARTIST *artistLookUp(node *head, unsigned int artistId)
{
    node *result;
    ARTIST artist;              // TEMP artist node
    artist.artistId = artistId;
    if( (result = find(head, &artist, cmp_artist)) != NULL )
		return result->data;        // return corresponding node in the list

	return NULL;
}

void artistDelete(node** headRef, unsigned int artistId)
{
	ARTIST art;
	art.artistId = artistId;
	removeNode(headRef, &art, cmp_artist);
}

/* ==================== */
/* Rooms list functions */

/*
 * Compare 2 ROOM instances
 */
int cmp_room(ROOM *a, ROOM *b)
{
    if( a == b ) return 0;
    else if( (*a).roomId > (*b).roomId ) return 1;
    else if((*a).roomId < (*b).roomId) return -1;
    return 0;
}

// print a single room instance
void roomPrint(ROOM *r)
{
	fprintf(stdout, "\t\tPaintings list of room: %u\n", r->roomId);
	roomPaintingsPrint(r->artPtr);
}

/*
 * Print a list of rooms
 */
void printRooms(node *head)
{
	fprintf(stdout, "List of Rooms\n");
	printContents(head, roomPrint);
}

/*
 * Create and insert a new room instance into the list of rooms pointed to by headRef
 * Arguments: roomId, the identification number of room
 */
void roomInsert(node **headRef, unsigned int roomId)
{
    ROOM *r = (ROOM *) malloc(sizeof(ROOM));
    node *container = (node *) malloc(sizeof(node));

    r->roomId = roomId;
    r->artPtr = NULL;

    container->data = r;
    container->next = NULL;

    SortedInsert(headRef, container, cmp_room);

}

/*
 * Search for a specific room in the rooms' list
 */
ROOM *roomLookup(node *head, unsigned int id)
{
	ROOM r;
	node *n = NULL;
	r.roomId = id;
	if( (n = sorted_list_lookup(head, &r, cmp_room)) != NULL)
		return n->data;
	return NULL;
}

/*
 * Delete a specific room instance from the rooms' list based to the identification number of the room
 */
void roomDelete(node **headRef, unsigned int roomId)
{
    ROOM *room = roomLookup(*headRef, roomId);
	DeleteList(&(room->artPtr));
    sorted_list_delete(headRef, room, cmp_room);
}

/* ============================ */
/* Room painting list functions */

/*
 * Compare 2 roomPainting instances
 * Takes 2 arguments a, b and returns 1 if a > b, 0 if a = b, -1 if b > a
 */
int cmp_roomPainting(ROOMPAINTING *a, ROOMPAINTING *b)
{
    if( a == b ) return 0;
    else if( (*a).paintingId == (*b).paintingId ) return 0;
    else if((*b).paintingId > (*a).paintingId) return -1;
    else return 1;
}

// helper function for printing a single instance of ROOMPAINTING
void printRoomPainting(ROOMPAINTING *rp)
{
	PAINTING *p = NULL;
	ARTIST *artistPtr = NULL;
	if( (p = paintingLookup(PaintingsList, rp->paintingId)) != '\0')
	{
            if (p->artistPtr != '\0') {
                artistPtr = p->artistPtr;
                fprintf(stdout, "\t\t\t< %u,  %s,  %u,  %s,  %u,  %s >\n",
                    p->paintingId, p->paintingName, artistPtr->artistId, artistPtr->artistName,
                    artistPtr->artStylePtr->artStyleId, artistPtr->artStylePtr->styleName);
            }
	}
}

// Print a list of roomPaintings
void roomPaintingsPrint(node *head)
{
    fprintf(stdout, "\t\t\tList of Paintings\n");
    printContents(head, printRoomPainting);
}

/*
 * Create and insert a single instance of roomPainting into a list of roomPaintings pointed to by headRef
 */
void roomPaintingInsert(node **headRef, unsigned int paintingId)
{
    ROOMPAINTING *r = (ROOMPAINTING *) malloc(sizeof(ROOMPAINTING));
    node *container = (node *) malloc(sizeof(node));

    r->paintingId = paintingId;
    container->data = r;
    container->next = NULL;
    SortedInsert(headRef, container, cmp_roomPainting);
}


/*
 * Search for a specific roomPainting into a list of roomPaintings based by it's identification number
 */
ROOMPAINTING *roomPaintingLookUp(node *head, unsigned int id)
{
    ROOMPAINTING rp;
    node *n = '\0';
    rp.paintingId = id;

	if( (n = sorted_list_lookup(head, &rp, cmp_roomPainting)) != '\0' )
		return n->data;

	return NULL;
}

/*
 * Delete a specific instance of roomPainting based by its identification number from a list pointed to by headRef
 */
void roomPaintingDelete(node **headRef, unsigned int paintingId)
{
    ROOMPAINTING rp;
    rp.paintingId = paintingId;
    sorted_list_delete(headRef, &rp, cmp_roomPainting);
}

/*
 * Move a specific painting from source room rsource to destination rdest
 * and properly change the registration at paintings list
 */
int roomPaintingMove(MUSEUM *mdest, ROOM *rdest, ROOM *rsource, unsigned int paintingId)
{
    ROOMPAINTING *rp = NULL;
    PAINTING *p = paintingLookup(PaintingsList, paintingId);

    if (p != '\0') {
        if ( (rp = roomPaintingLookUp(rsource->artPtr, paintingId)) != '\0') {
            roomPaintingInsert(&rdest->artPtr, rp->paintingId);
            roomPaintingDelete(&rsource->artPtr, rp->paintingId);
            p->roomPtr = rdest;     // remember to change the painting attributes in main painting list also
            p->museumPtr = mdest;
            return 1;
        }
    }
    return 0;
}

/* ====================== */
/* painting list functions */
/*
 * Insert a new painting in a room
 * @param ID of painting to insert
 * @param name of the painting
 * @param ID of the corresponding artist
 * @param museum which the room belongs to
 * @param room which the painting belongs to
 */
void paintingInsert(unsigned int paintingId, char paintingName[MAX_NAME_LENGTH], unsigned int artistId, MUSEUM *m, ROOM *r)
{
	PAINTING *p = malloc(sizeof(PAINTING));
	p->paintingId = paintingId;
	strncpy(p->paintingName, paintingName, MAX_NAME_LENGTH);
	p->prev = NULL;
	p->next = NULL;
	p->artistPtr = artistLookUp(ArtistsList, artistId);
	p->museumPtr = m;
	p->roomPtr = r;
	paintingPush(&PaintingsList, p);

}


/* ====================== */
/* Museums list functions */

/*
 * Compare 2 museum instances
 * Takes 2 arguments a, b and returns 1 if a > b, 0 if a = b, -1 if b > a
 */
int cmp_museum(MUSEUM *a, MUSEUM *b)
{
    if( a == b ) return 0;
    else if( (*a).museumId > (*b).museumId ) return 1;
    else if( (*a).museumId == (*b).museumId ) return 0;
    else return -1;
}

// helper printing function which print a single museum instance
void printMuseum(MUSEUM *m)
{
    unsigned short i;
    fprintf(stdout, "< %u, %s, %s >\n", m->museumId, m->museumName, m->museumAddress);
    for(i=0; i < m->currentFunctionalFloors; ++i)
    {
        fprintf(stdout, "\tFL[%u]: ", i);
        printRooms(m->FL[i]);
    }
}

/*
 * Trigger function for printing the list of museums
 */
void museumsPrint(node *head)
{
    fprintf(stdout, "List of Museums\n");
    printContents(head, printMuseum);
}

void add_new_floor(MUSEUM *m, unsigned short floor, unsigned int roomQuantity, unsigned int *roomIds)
{
    int i;
    if( m == NULL ) fprintf(stderr, "add_new_floor: request for a new floor into empty space\n");
    else
    {
        if( (m->currentFunctionalFloors > MAX_FLOORS_NUM) || (floor > MAX_FLOORS_NUM) )
            fprintf(stdout, "Museum %s has reached it's maximum number of floors\n", m->museumName);
        else if(m->FL[floor] == NULL) {
            for(i=0; i < roomQuantity; ++i) {
                roomInsert(&m->FL[floor], roomIds[i]);
            }
            m->currentFunctionalFloors += 1;
        } else fprintf(stdout, "Floor [%u] is not empty!\n", floor);
    }
}

/*
 * Create and insert a new museum instance
 */
node* museumInsert(node** headRef, unsigned int museumId, char museumName[MAX_NAME_LENGTH], char museumAddress[MAX_NAME_LENGTH])
{
    unsigned short i;
    MUSEUM *new = (MUSEUM *) malloc(sizeof(MUSEUM));
    node *container = (node *) malloc (sizeof(node));
    new->museumId = museumId;
    new->currentFunctionalFloors = 0;
    strncpy (new->museumAddress, museumAddress, MAX_NAME_LENGTH);
    strncpy (new->museumName, museumName, MAX_NAME_LENGTH);
    for(i=0; i <= MAX_FLOORS_NUM; ++i)
    	new->FL[i] = NULL;
    container->data = new;
    container->next = NULL;
    SortedInsert(headRef, container, cmp_museum);
    fprintf(stdout, "Successfuly inserted museum %s\n", museumName);
    return container;

}

/*
 * Search for a specific museum
 */
MUSEUM *museumLookup(node *head, unsigned int museumId)
{
    MUSEUM m;
    node *n = '\0';
    m.museumId = museumId;
    if( (n = sorted_list_lookup(head, &m, cmp_museum)) != '\0' )
        return n->data;

    return NULL;
}

/*
 * Delete a museum instance
 */
void museumDelete(node **headRef, unsigned int museumId)
{
    MUSEUM m;
    m.museumId = museumId;
    sorted_list_delete(headRef, &m, cmp_museum);
}

/* ========================= */
/* Complex command functions */
void commandN(unsigned int paintingId, char paintingName[MAX_NAME_LENGTH],
			  unsigned int artistId, unsigned int museumId, unsigned short floorNum, unsigned int roomId)
{
    ROOM *r;
    MUSEUM *m = museumLookup(MuseumsList, museumId);

    if(m != '\0')
    {
        if(m->FL[floorNum] != NULL)
        {
            r = roomLookup(m->FL[floorNum], roomId);
            if( r ) {
                roomPaintingInsert(&r->artPtr, paintingId);
                paintingInsert(paintingId, paintingName, artistId, m, r);
            }
        } else fprintf(stdout, "No %u floor exists!\n", floorNum);
    } else fprintf(stdout, "No museum found\n");

}

void commandV(unsigned int paintingId, unsigned int museumId_s, unsigned short floorNum_s, unsigned int roomId_s, unsigned int museumId_dest, unsigned short floorNum_dest, unsigned int roomId_dest)
{
    MUSEUM *m_s = museumLookup(MuseumsList, museumId_s);
    MUSEUM *m_dest = museumLookup(MuseumsList, museumId_dest);
    ROOM *r_s = NULL, *r_dest = NULL;

    if( m_s != '\0' && m_dest != '\0' )
    {
        // Check if the floor-room arguments are correct
        if( m_dest->FL[floorNum_dest]  != '\0' )
            r_dest = roomLookup(m_dest->FL[floorNum_dest], roomId_dest);
        if( m_s->FL[floorNum_s] != '\0' )
            r_s = roomLookup(m_s->FL[floorNum_s], roomId_s);

        // if we found such rooms, then move the painting
        if (r_s!='\0' && r_dest != '\0') {
            roomPaintingMove(m_dest, r_dest, r_s, paintingId);
            fprintf(stdout, "Transfer of painting %u to room %u of museum %u DONE\n", paintingId, roomId_dest, museumId_dest);
        }
    }
}

void commandL(unsigned int museumId_s, unsigned short floorNum_s, unsigned int roomId_s, unsigned int museumId_dest, unsigned short floorNum_dest, unsigned int roomId_dest)
{
    MUSEUM *m_s = museumLookup(MuseumsList, museumId_s);
    MUSEUM *m_dest = museumLookup(MuseumsList, museumId_dest);
    ROOM *r_s = '\0', *r_dest = '\0';
    ROOMPAINTING *rp;
    node *current;

    if( m_s != '\0' && m_dest != '\0' )
    {
        // Check if the floor-room arguments are correct
        if( m_dest->FL[floorNum_dest]  != '\0' )
            r_dest = roomLookup(m_dest->FL[floorNum_dest], roomId_dest);
        if( m_s->FL[floorNum_s] != '\0' )
            r_s = roomLookup(m_s->FL[floorNum_s], roomId_s);

        // if we found such rooms, then move the painting
        if ( r_s != '\0' && r_dest != '\0')
        {
            for (current=r_s->artPtr; current; current = current->next) {
                rp = (ROOMPAINTING *) current->data;
                roomPaintingMove(m_dest, r_dest, r_s, rp->paintingId);
            }
        }
    }
}

void commandG(unsigned int roomId_src, unsigned int roomId_dest)
{
	node *current, *currentP;
	MUSEUM *msource, *mdest;
	unsigned short i, source_flag=0, source_floor, dest_flag=0, total_flag = 0;
	ROOM *rsource, *rdest;

	// Traverse through the museums list
	for (current = MuseumsList; current; current = current->next)
	{
            if (!source_flag)	// have we found source museum yet?
            {   // no
                msource = (MUSEUM *) current->data;
                for (i = 0; i < msource->currentFunctionalFloors; ++i) {				// request for room with roomId_src
                    if ( (rsource = roomLookup(msource->FL[i], roomId_src)) != NULL)
                    {
                        source_floor = i;   // We found OUR ROOM! YEY!
                        source_flag = 1;
                        break;
                    }
                }
            }
            if (!dest_flag)		// have we found our destination museum yet?
            {
                mdest = (MUSEUM *) current->data;
                for (i = 0; i < mdest->currentFunctionalFloors; ++i) {			// request for room with roomId_dest
                    if ( (rdest = roomLookup(mdest->FL[i], roomId_dest)) != NULL)
                    {
                        dest_flag = 1;     // We found destination room! YEY!
                        break;
                    }
                }
            }
            if ( source_flag && dest_flag )
            {   // We have the right rooms so we can exchange roompaintings
                for ( currentP = rsource->artPtr; currentP; currentP = currentP->next) {
                        ROOMPAINTING *rp = (ROOMPAINTING *) currentP->data;
                        roomPaintingMove(mdest, rdest, rsource, rp->paintingId);
                }
                total_flag = 1;	// set successful flag
                break;
            }
	}
	if( total_flag ) {		// if successful flag delete source room
            roomDelete(&msource->FL[source_floor], roomId_src);
            fprintf(stdout, "Event G completed successfully\n");
	} else {
            fprintf(stderr, "Event G did not workout for some reason\n");
	}

}

/*
 * if the current roompainting's artist is the required artist specified by the artistId argument,
 * then move the current room painting to ROOM *rdest of the MUSEUM *mdest
 */
void commandI(unsigned int artistId, MUSEUM *mdest, ROOM *rdest, ROOM *rsource, ROOMPAINTING *rp)
{
	PAINTING *p;
	if ( (p = paintingLookup(PaintingsList, rp->paintingId)) != '\0' )
	{
            if (p->artistPtr->artistId == artistId) {
                roomPaintingMove(mdest, rdest, rsource, p->paintingId);
            }
	}
}

/*
 * if the current roompainting's art style is the required art style specified by the artStyleId argument,
 * then move the current room painting to ROOM *rdest of the MUSEUM *mdest
 */
void commandY(unsigned int artStyleId, MUSEUM *mdest, ROOM *rdest, ROOM *rsource, ROOMPAINTING *rp)
{
    PAINTING *p;
    if ( (p = paintingLookup(PaintingsList, rp->paintingId)) != '\0')
    {
        if ( p->artistPtr->artStylePtr->artStyleId == artStyleId) {
            roomPaintingMove(mdest, rdest, rsource, rp->paintingId);
        }
    }
}


/**
 * get all the paintings from every room, of every floor of every museum and apply the function
 * (*func)(ROOMPAINTING *) to it
 */
void searchPaintings(unsigned int id, unsigned int mdest, unsigned int floorNum_dest, unsigned int rdest, void (*func)(unsigned int, MUSEUM *, ROOM *, ROOM *, ROOMPAINTING *))
{
    node *currentM, *currentR, *currentP;
    MUSEUM *m, *m_dest = museumLookup(MuseumsList, mdest);
    ROOM *r, *r_dest;
    unsigned short i;

    // check for valid arguments
    if (m_dest == '\0') {
        fprintf(stderr, "No museum with ID: %u found!\n", mdest);
        return;
    }
    if ( (r_dest = roomLookup(m_dest->FL[floorNum_dest], rdest)) == '\0') {
        fprintf(stderr, "No room with id %u found on floor %u\n", rdest, floorNum_dest);
        return;
    }

    // Traverse through every museum
    for (currentM = MuseumsList; currentM; currentM = currentM->next) {
        m = (MUSEUM *) currentM->data;
        for (i=0; i < m->currentFunctionalFloors; ++i) {
            for (currentR = m->FL[i]; currentR; currentR = currentR->next) {
                r = (ROOM *) currentR->data;
                for (currentP = r->artPtr; currentP; currentP = currentP->next) {
                    func(id, m_dest, r_dest, r, (ROOMPAINTING *)currentP->data);
                }
            }
        }
    }
}

/*
 * Clean up program resources!
 */
void destructor()
{
    node *currentM, *currentR;
    MUSEUM *m;
    ROOM *r;
    unsigned short i;

    for (currentM = MuseumsList; currentM; currentM = currentM->next) {
        m = (MUSEUM *) currentM->data;
        for (i=0; i < m->currentFunctionalFloors; ++i) {
            for (currentR = m->FL[i]; currentR; currentR = currentR->next) {
                r = (ROOM *) currentR->data;
                DeleteList(&r->artPtr);
            }
            DeleteList(&m->FL[i]);
        }
    }
    DeleteList(&ArtStylesList);
    DeleteList(&ArtistsList);
    DeletePaintingsList(&PaintingsList);
    DeleteList(&MuseumsList);
}
