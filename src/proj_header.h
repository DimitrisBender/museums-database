/* 
 * File:   proj_header.h
 * Author: Dimitrios Lygizos
 *
 * Created on October 24, 2014, 12:42 PM
 */

#ifndef PROJ_HEADER_H
#define PROJ_HEADER_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "linked_list.h"

#define LINE_LENGTH 	1024	/* Maximum length of a line in input file */
#define MAX_NAME_LENGTH	30		/* Maximum length of names */
#define MAX_FLOORS_NUM	5		/* Maximum number of floors per museum*/

typedef struct ArtStyle { 		/* Item of art styles list */
	unsigned int artStyleId;
	char styleName[MAX_NAME_LENGTH];
} ARTSTYLE;

typedef struct Artist {			/* Item of artists list */
	unsigned int artistId;
	char artistName[MAX_NAME_LENGTH];
	unsigned int YearOfBirth;
	int YearOfDeath;
	ARTSTYLE *artStylePtr;
} ARTIST;

typedef struct RoomPainting{	/* Item of works of art of some room*/
	int paintingId;
} ROOMPAINTING;

typedef struct Room { 			/* Item of rooms list*/
	int roomId;
	node *artPtr;
} ROOM;

typedef struct Museum{ 			/* Item of museums list*/
	int museumId;
	unsigned short currentFunctionalFloors;
	char museumName[MAX_NAME_LENGTH];
	char museumAddress[MAX_NAME_LENGTH];
	struct List_Node *FL[MAX_FLOORS_NUM+1];
} MUSEUM;

typedef struct Painting { 		/* Item of paintings list*/
	int paintingId;
	char paintingName[MAX_NAME_LENGTH];
	ARTIST *artistPtr;
	MUSEUM *museumPtr;
	ROOM *roomPtr;
	struct Painting *next;
	struct Painting *prev;
} PAINTING;

/* */
node *ArtStylesList;	/* List of art styles */
node *ArtistsList;		/* List of artists */
node *MuseumsList;		/* List of museums */
PAINTING *PaintingsList;	/* List of paintings */

/* */
/* Controller functions */
ARTSTYLE *artStyleLookUp(node*, unsigned int);			// search for an artStyle
void artStyleInsert(node **, unsigned int, char *);		// insert new artStyle instance
void artStylesPrint(node*);								// print ALL the artStyle in the list pointed to by the node argument
void artStyleDelete( node**, unsigned int );			// Delete a specific artStyle instance from the list pointed to by the node argument
ARTIST *artistLookUp(node *, unsigned int );			// search for a specific artist
void artistsPrint(node*);								// Print ALL artists in the list pointed to by the node argument
void artistInsert(node **, unsigned int, unsigned int, int, unsigned int, node *, char[MAX_NAME_LENGTH]);	// insert new artist
void artistDelete(node **, unsigned int);				// Delete a specific artist instance from the list pointed to by the node argument
ROOM *roomLookup(node *, unsigned int);					// search for a specific room
void roomInsert(node **, unsigned int);					// insert new Room instance
void roomDelete(node **, unsigned int );				// Delete a room instance
void roomPrint(ROOM *);									// print a single ROOM instance
void printRooms(node *);								// print every room in the list
ROOMPAINTING *roomPaintingLookUp(node *, unsigned int);	// search for a specific roomPainting in a room
// move a specific roomPainting based by the paintingId argument from rsource to rdest
int roomPaintingMove(MUSEUM *mdest, ROOM *rdest, ROOM *rsource, unsigned int paintingId);
void roomPaintingsPrint(node *);						// print ALL roomPaintings in a room
void roomPaintingInsert(node **, unsigned int );		// insert new roomPainting in a room
void roomPaintingDelete(node **, unsigned int);			// Delete a roompainting
// insert new painting
void paintingInsert(unsigned int paintingId, char paintingName[MAX_NAME_LENGTH], unsigned int artistId, MUSEUM *m, ROOM *r);
MUSEUM *museumLookup(node *, unsigned int);				// Search for a specific museum
void printMuseum(MUSEUM *);								// print a single museum instance
void museumsPrint(node *);								// print ALL  MUSEUMS in a list
// insert new museum
node* museumInsert(node**, unsigned int, char [MAX_NAME_LENGTH], char [MAX_NAME_LENGTH]);
void museumDelete(node **, unsigned int );				// Delete a museum
void add_new_floor(MUSEUM *, unsigned short, unsigned int, unsigned int *);		// add a new floor in the museum passed as argument with given number of rooms
/* */
node *sorted_list_lookup(node *, void *, int (*compare) (void *, void *));		// search a sorted list
void sorted_list_delete(node **, void *, int (*cmpfunc) (void *, void *));		// delete an instance from a sorted list
void searchPaintings(unsigned int id, unsigned int mdest, unsigned int floorNum_dest, unsigned int rdest, void (*func)(unsigned int , MUSEUM *, ROOM *, ROOM *, ROOMPAINTING *)); // for every room in every floor of every museum
void destructor();	// FREE EVERYTHING

/* */
/* Complex commands */
void commandG(unsigned int roomId_src, unsigned int roomId_dest);
void commandN(unsigned int paintingId, char paintingName[MAX_NAME_LENGTH], unsigned int artistId, unsigned int museumId, unsigned short floorNum, unsigned int roomId);
void commandV(unsigned int, unsigned int , unsigned short , unsigned int , unsigned int , unsigned short , unsigned int );
void commandL(unsigned int museumId_s, unsigned short floorNum_s, unsigned int roomId_s, unsigned int museumId_dest, unsigned short floorNum_dest, unsigned int roomId_dest);
void commandI(unsigned int artistId, MUSEUM *mdest, ROOM *roomdest, ROOM *roomsource, ROOMPAINTING *rp);
void commandY(unsigned int artStyleId, MUSEUM *dest, ROOM *rdest, ROOM *rsource, ROOMPAINTING *rp);

/* */
/* Double-link list */

/*
 * Insert a new painting to the collection
 */
void paintingPush(PAINTING **, PAINTING *p);

/*
 * Delete a painting from the collection
 */
void paintingDelete(PAINTING **, unsigned int);

/*
 * Print the painting collection
 */
void paintingsPrint(PAINTING *);

/*
 * Search for a specific painting from the collection
 */
PAINTING *paintingLookup(PAINTING *, unsigned int);

void DeletePaintingsList(PAINTING **);

#ifdef __cplusplus
extern "C" {
#endif




#ifdef __cplusplus
}
#endif

#endif /* PROJ_HEADER_H */

