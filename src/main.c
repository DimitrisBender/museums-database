/* 
 * File:   main.c
 * Author: Dimitris Lygizos
 *
 * Created on October 24, 2016, 12:44 PM
 */
#include "proj_header.h"
#include <stdio.h>
#include <stdlib.h>

/*
 * function main for museums project (lesson CS240)
 */
int main(int argc, char** argv)
{
    FILE *fin = NULL;
    char line[LINE_LENGTH], event, *delim=" ";
    int i,j;
    PaintingsList = malloc(sizeof(PAINTING));
    PaintingsList->prev = NULL;
    PaintingsList->next = NULL;
    PaintingsList->museumPtr = NULL;
    PaintingsList->roomPtr = NULL;
    PaintingsList->artistPtr = NULL;
    PaintingsList->paintingId = 0;
    strcpy(PaintingsList->paintingName, "Guard Node");
    /* Check command line arguments */
    if ( argc != 2 )	{
            printf("Usage: %s <input_file> \n", argv[0]);
            return EXIT_FAILURE;
    }

    /* Open input file */
    if (( fin = fopen(argv[1], "r") ) == NULL ) {
            printf("\n Could not open file: %s\n", argv[1]);
            return EXIT_FAILURE;
    }

    /* Read input file line-by-line and handle the events */
    while( fgets(line, LINE_LENGTH, fin) != NULL ) {

            /* Un-comment the line below when in debug mode */
            //printf("Event: %s \n", line);

            switch(line[0]) {

                            /* Add a new art style:
                             * S <artStyleId> <styleName> */
                    case 'S':
                    {
                        unsigned int artStyleId;
                        char styleName[MAX_NAME_LENGTH];
                        sscanf(line, "%c %u %s", &event, &artStyleId, styleName);

                        /* Un-comment the line below when in debug mode */
                        //printf("%c %u %s\n", event, artStyleId, styleName);

                        /* put your code here */
                        artStyleInsert(&ArtStylesList, artStyleId, styleName);
                        break;
                    }
                            /* Add a new artist:
                             * A <artistId> <artistName> <YearOfBirth> <YearOfDeath> <artStyleId> */
                    case 'A':
                    {
                        unsigned int artistId, YearOfBirth, artStyleId;
                        int YearOfDeath;
                        char artistName[MAX_NAME_LENGTH];
                        sscanf(line, "%c %u %s %u %d %u", &event, &artistId, artistName, &YearOfBirth, &YearOfDeath, &artStyleId);

                        /* Un-comment the line below when in debug mode */
                        //printf("%c %u %s %u %d %u\n", event, artistId, artistName, YearOfBirth, YearOfDeath, artStyleId);

                        /* put your code here */
                        artistInsert(&ArtistsList, artistId, YearOfBirth, YearOfDeath, artStyleId, ArtStylesList, artistName);
                        break;
                    }

                            /* Add a new museum:
                             * M <museumId> <museumName> <museumAddress> <f>
                             * <rooms0> <roomId0-1> ... <roomId0-<r0>>
                             * <rooms1> <roomId1-1> ... <roomId1-<r1>>
                             * .
                             * <rooms<f>> <roomId<f>-1> ... <roomId<f>-<r<f>>> */
                    case 'M':
                    {
                        unsigned int museumId, f, **roomIds, *roomNums;
                        char museumName[MAX_NAME_LENGTH], museumAddress[MAX_NAME_LENGTH];
                        sscanf(line, "%c %u %s %s %u", &event, &museumId, museumName, museumAddress, &f);
                        roomIds = (unsigned int **) malloc ((f+1)*sizeof(unsigned int *));  // CHANGE: f --> f+1
                        roomNums = (unsigned int *) malloc ((f+1)*sizeof(unsigned int));    // CHANGE: f --> f+1

                        /* Un-comment the line below when in debug mode */
                        printf("%c %u %s %s %u", event, museumId, museumName, museumAddress, f);

                        for (i=0; i<=f; i++) {							/* CHANGE: i<f --> i<=f */
                            fgets(line, LINE_LENGTH, fin);					/* CHANGE: ADDED */

                            roomNums[i] = atoi(strtok(line, delim));			/* CHANGE: strtok(NULL, delim) -->  strtok(line, delim)*/
                            roomIds[i] = (unsigned int *) malloc (roomNums[i]*sizeof( unsigned int));

                            /* Un-comment the line below when in debug mode */
                            printf("\n   %u ", roomNums[i]);

                            for (j=0; j<roomNums[i]; j++) {
                                roomIds[i][j] = atoi(strtok(NULL, delim));

                                /* Un-comment the line below when in debug mode */
                                printf("%u ", roomIds[i][j]);
                            }
                        }
                        /* Un-comment the line below when in debug mode */
                        printf("\n");

                        /* put your code here */
                        node *n = museumInsert(&MuseumsList, museumId, museumName, museumAddress);
                        for(i=0; i <= f; ++i)
                                add_new_floor(n->data, i, roomNums[i], roomIds[i]);

                        // museumsPrint(MuseumsList);
                        // fprintf(stdout, "M  %u,  %s,  %s  DONE\n", museumId, museumName, museumAddress);
                        break;
                    }

                            /* Add another floor to some museum:
                             * O <museumId> <r> <roomId1> ... <roomId<r>> */
                    case 'O':
                    {
                            unsigned int museumId, r, *roomIds;
                            sscanf(line, "%c %u %u", &event, &museumId, &r);
                            roomIds = (unsigned int *) malloc (r*sizeof(unsigned int));

                            /* Un-comment the line below when in debug mode */
                            //printf("%c %u %u\n", event, museumId, r);

                            strtok(line, delim);
                            for (i=0; i < 2; i++)
                                    strtok(NULL, delim);

                            for (i=0; i<r; i++) {
                                    roomIds[i] = atoi(strtok(NULL, delim));

                                    /* Un-comment the line below when in debug mode */
                                    //printf("%d ", roomIds[i]);
                            }
                            /* Un-comment the line below when in debug mode */
                            //printf("\n");

                            /* put your code here */
                            MUSEUM *m = museumLookup(MuseumsList, museumId);	// retrieve the right museum instance
                            if( m ) {                                           // add a new floor into the museum
                                // floor is currentFunctionFloor-1 as we start counting from 0
                                add_new_floor(m, m->currentFunctionalFloors, r, roomIds);
                                printMuseum(m);
                            } else fprintf(stderr, "Museum with id %u does not exist!\n", museumId);

                            fprintf(stdout, "O  %u,  %u ", museumId, r);
                            for (i=0; i<r; ++i)
                                    fprintf(stdout, " %u ", roomIds[i]);

                            fprintf(stdout, "DONE\n");
                            break;
                    }

                            /* Add another room to a floor of some museum:
                             * R <museumId> <floorNum> <roomId> <roomId_src> <flag> */
                    case 'R':
                    {
                        unsigned int museumId, floorNum, roomId, roomId_src;
                        int flag, i;
                        MUSEUM *m, *m_src;
                        ROOM *r_s=NULL, *r_dest=NULL;
                        node *current;
                        ROOMPAINTING *rp;
                        PAINTING *p;
                        sscanf(line, "%c %d %d %d %d %d", &event, &museumId, &floorNum, &roomId, &roomId_src, &flag);

                        /* Un-comment the line below when in debug mode */
                        //printf("%c %d %d %d %d %d\n", event, museumId, floorNum, roomId, roomId_src, flag);

                        /* put your code here */
                        if (floorNum > MAX_FLOORS_NUM) {	// invalid floorNum argument
                                fprintf(stderr, "floorNum has invalid index, should be 0 <= floorNum <= %u\n", MAX_FLOORS_NUM);
                        }
                        else if( (m = museumLookup(MuseumsList, museumId)) != '\0')
                        {	// search for the destination museum in the museum list
                            roomInsert(&m->FL[floorNum], roomId);           // insert new room
                            r_dest = roomLookup(m->FL[floorNum], roomId);   // get new room pointer
                            for(current = MuseumsList; current; current=current->next)
                            {
                                m_src = (MUSEUM *) current->data;   // search for the SOURCE ROOM in MuseumsList
                                for (i=0; i < m_src->currentFunctionalFloors; ++i) {
                                    if ( (r_s = roomLookup(m_src->FL[i], roomId_src)) != '\0' ) break;
                                }
                                if (r_s) {
                                    break;
                                }
                            }
                            if( r_s != '\0' && r_dest != '\0' )
                            {
                                current = r_s->artPtr;
                                if( flag < 0 )
                                {
                                    i=0;
                                    while(i <= Length(r_s->artPtr)/2) {
                                        rp = (ROOMPAINTING *) current->data;
                                        current=current->next;
                                        roomPaintingMove(m, r_dest, r_s, rp->paintingId);
                                        ++i;
                                    }
                                } else {
                                    while (current) {
                                        rp = (ROOMPAINTING *) current->data;
                                        current = current->next;
                                        if( (p = paintingLookup(PaintingsList, rp->paintingId)) != '\0')
                                            if(p->artistPtr->artStylePtr->artStyleId == flag)
                                                roomPaintingMove(m, r_dest, r_s, rp->paintingId);
                                    }
                                }
                                fprintf(stdout, "R  %u,  %u,  %u,  %u,  %d  DONE\n",
                                            museumId, floorNum, roomId, roomId_src, flag);
                            }
                        } else {
                            fprintf(stderr, "No museum with ID %u found!\n", museumId);
                        }

                        break;
                    }

                            /* Add a painting:
                             * N <paintingId> <paintingName> <artistId> <museumId> <floorNum> <roomid> */
                    case 'N':
                    {
                        unsigned int paintingId, artistId, museumId, floorNum, roomId;
                        char paintingName[MAX_NAME_LENGTH];
                        sscanf(line, "%c %d %s %d %d %d %d", &event, &paintingId, paintingName, &artistId, &museumId, &floorNum, &roomId);

                        /* Un-comment the line below when in debug mode */
                        //printf("%c %u %s %u %u %u %u\n", event, paintingId, paintingName, artistId, museumId, floorNum, roomId);

                        /* put your code here */
                        commandN(paintingId, paintingName, artistId, museumId, floorNum, roomId);
                        fprintf(stdout, "N  %u,  %s,  %u,  %u,  %u,  %u  DONE\n",
                                        paintingId, paintingName, artistId, museumId, floorNum, roomId);
                        paintingsPrint(PaintingsList);
                        break;
                    }

                            /* Move a painting from one room to another room (of a possibly different floor and/or museum):
                             * V <paintingID> <museumId> <floorNum> <roomId> <museumId_dest> <floorNum_dest> <roomId_dest> */
                    case 'V':
                    {
                        unsigned int paintingId, museumId_s, floorNum_s, roomId_s, museumId_dest, floorNum_dest, roomId_dest;
                        sscanf(line, "%c %d %d %d %d %d %d %d", &event, &paintingId, &museumId_s, &floorNum_s, &roomId_s, &museumId_dest, &floorNum_dest, &roomId_dest);

                        /* Un-comment the line below when in debug mode */
                        //printf("%c %d %d %d %d %d %d %d\n", event, paintingId, museumId, floorNum, roomId, museumId_dest, floorNum_dest, roomId_dest);

                        /* put your code here */
                        commandV(paintingId, museumId_s, floorNum_s, roomId_s, museumId_dest, floorNum_dest, roomId_dest);
                        fprintf(stdout, "V  %u,  %u,  %u,  %u,  %u,  %u,  %u  DONE\n",
                                        paintingId, museumId_s, floorNum_s, roomId_s, museumId_dest, floorNum_dest, roomId_dest);
                        break;
                    }

                            /* Move all paintings from one room to another room (of a possibly different floor and/or museum):
                             * L <museumId> <floorId> <roomId> <museumId_dest> <floorId_dest> <roomId_dest> */
                    case 'L':
                    {
                        unsigned int museumId, floorNum, roomId, museumId_dest, floorNum_dest, roomId_dest;
                        sscanf(line, "%c %u %u %u %u %u %u", &event, &museumId, &floorNum, &roomId, &museumId_dest, &floorNum_dest, &roomId_dest);

                        /* Un-comment the line below when in debug mode */
                        //printf("%c %u %u %u %u %u %u\n", event, museumId, floorNum, roomId, museumId_dest, floorNum_dest, roomId_dest);

                        /* put your code here */
                        commandL(museumId, floorNum, roomId, museumId_dest, floorNum_dest, roomId_dest);
                        fprintf(stdout, "L  %u,  %u,  %u,  %u,  %u,  %u  DONE\n",
                                        museumId, floorNum, roomId, museumId_dest, floorNum_dest, roomId_dest);
                        break;
                    }

                            /* Merge rooms of the the same museum and the same floor:
                             * G <roomId> <roomId_dest> */
                    case 'G':
                    {
                        unsigned int roomId, roomId_dest;
                        sscanf(line, "%c %u %u", &event, &roomId, &roomId_dest);
                        /* Un-comment the line below when in debug mode */
                        //printf("%c %u %u\n", event, roomId, roomId_dest);

                        /* put your code here */
                        commandG(roomId, roomId_dest);
                        break;
                    }

                            /* Temporal painting exhibition of some artist:
                             * I <artistId> <museum�d_dest> <floorNum_dest> <roomid_dest> */
                    case 'I':
                    {
                        unsigned int artistId, museumId_dest, floorNum_dest, roomId_dest;
                        sscanf(line, "%c %u %u %u %u", &event, &artistId, &museumId_dest, &floorNum_dest, &roomId_dest);

                        /* Un-comment the line below when in debug mode */
                        //printf("%c %u %u %u %u\n", event, artistId, museumId_dest, floorNum_dest, roomId_dest);

                        /* put your code here */
                        searchPaintings(artistId, museumId_dest, floorNum_dest, roomId_dest, commandI);
                        fprintf(stdout, "I  %u  %u  %u  %u  DONE\n", artistId, museumId_dest, floorNum_dest, roomId_dest);
                        break;
                    }

                            /* Temporal painting exhibition of some art style:
                             * Y <artStyleId> <museum�d_dest> <floorNum_dest> <roomid_dest> */
                    case 'Y':
                    {
                        unsigned int artStyleId, museumId_dest, floorNum_dest, roomId_dest;
                        sscanf(line, "%c %d %d %d %d", &event, &artStyleId, &museumId_dest, &floorNum_dest, &roomId_dest);

                        /* Un-comment the line below when in debug mode */
                        //printf("%c %d %d %d %d\n", event, artStyleId, museumId_dest, floorNum_dest, roomId_dest);

                        /* put your code here */
                        searchPaintings(artStyleId, museumId_dest, floorNum_dest, roomId_dest, commandY);
                        fprintf(stdout, "Y  %u  %u  %u  %u  DONE\n",
                                        artStyleId, museumId_dest, floorNum_dest, roomId_dest);
                        break;
                    }

                            /* Print works of art of some room:
                             * T <museumId> <floorNum> <roomId> */
                    case 'T':
                    {
                        unsigned int museumId, floorNum, roomId;
                        unsigned short i;
                        ROOM *r;
                        sscanf(line, "%c %u %u %u", &event, &museumId, &floorNum, &roomId);

                        /* Un-comment the line below when in debug mode */
                        //printf("%c %d %d %d\n", event, museumId, floorNum, roomId);

                        /* put your code here */
                        MUSEUM *m = museumLookup(MuseumsList, museumId);
                        if( m == '\0' ) fprintf(stderr, "T event: Museum with ID %u not found!\n", museumId);
                        else {
                            for (i=0; i < m->currentFunctionalFloors; ++i) {
                                if( (r = roomLookup(m->FL[i], roomId)) != '\0' )
                                {
                                    roomPrint(r);
                                }
                            }
                            fprintf(stdout, "T %u  %s  %u  %u  DONE\n", museumId, m->museumName, floorNum, roomId);
                        }
                        break;
                    }

                    /* Print system's data structures:
                     * P */
                    case 'P':
                    {
                        sscanf(line, "%c", &event);

                        /* Un-comment the line below when in debug mode */
                        //printf("%c\n", event);

                        /* put your code here */
                        museumsPrint(MuseumsList);
                        fprintf(stdout, "P DONE\n");
                        break;
                    }

                    /* Ignore everything else */
                    default:
                        /* Uncomment the line below when in debug mode */
                        printf("---Ignoring line: %s \n", line);

                        break;
            }
    }
    destructor();
    return (EXIT_SUCCESS);
}
